
import os
import time
import sys

import requests

TOP_20_COUNTRY = ("CN IN US ID BR PK NG BD RU JP"
                  " MX PH VN ET EG DE IR TR CD FR").split()

BASE_URL = "https://flupy.org/data/flag"
DEST_DIR = "flag_downloads"


def save_flag(image, file_name):
    """
    :param image: downloaded image
    :param file_name: file_name with .gif to be saved
    :return:
    """
    path = os.path.join(DEST_DIR, file_name)
    with open(path, 'wb') as fp:
        fp.write(image)


# def show(text):
#     print(text, end=" ")
#     sys.stdout.flush()


def get_flag(country):
    """
    :param country: country code string of 2 digits
    :return:
    """
    url = "{}/{cc}/{cc}.gif".format(BASE_URL, cc=country.lower())
    response = requests.get(url)
    return response.content


def download_many(country_list):
    """
    :param country_list: List of country names for which flag to be download
    :return:
    """
    for country in sorted(country_list):
        image = get_flag(country)
        # show(image)
        save_flag(image, country.lower() + ".gif")
    return len(TOP_20_COUNTRY)


def main(download_many):
    """
    :param download_many: Function to be called for downloading flags
    :return:
    """
    initial_time = time.time()
    total_flags = download_many(TOP_20_COUNTRY)
    time_elapsed = time.time() - initial_time
    msg = "\n {} flags downloaded in {:.3f}s"
    print(msg.format(total_flags, time_elapsed))


if __name__ == "__main__":
    main(download_many)