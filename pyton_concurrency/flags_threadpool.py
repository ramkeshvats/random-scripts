
# coding=utf-8

import os
import sys
import time

from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
from argparse import ArgumentParser
import requests

# from .flags import get_flag, save_flag

TOP_20_COUNTRY = ("CN IN US ID BR PK NG BD RU JP"
                  " MX PH VN ET EG DE IR TR CD FR").split()

BASE_URL = "https://flupy.org/data/flag"
DEST_DIR = "flag_downloads"
MAX_WORKERS = 20


class ArgParse(object):

    def __init__(self):
        parser = ArgumentParser(description="Process the given urls by downloaded data from them")
        parser.add_argument('-u', '--urls', help='Urls to be downloads')
        args = parser.parse_args()
        self.urls = args.urls
        # return urls.split()

    def get_input_urls(self):
        print('Please pass args! :(')
        return self.urls.split() if self.urls else []


def download_item(url):
    res = requests.get(url)
    print(res.status_code)
    return res.content


def download_many(urls):
    workers = min(MAX_WORKERS, len(urls))
    with ThreadPoolExecutor(workers) as executor:
        res = executor.map(download_item, urls)
        # print(res)

    # For comparision with sequential uncomment
    # these two lines and run again
    for i in urls:
        res = download_item(i)
    return len(list(res))

def main(download_function, urls):
    initial_time = time.time()
    total_flags = download_function(urls)
    time_elapsed = time.time() - initial_time
    msg = "\n {} flags downloaded in {:.3f}s"
    print(msg.format(total_flags, time_elapsed))


if __name__ == "__main__":
    parser = ArgParse()
    urls = parser.get_input_urls()
    print('Parsed!')
    main(download_many, urls)