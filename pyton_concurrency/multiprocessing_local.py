__author__ = 'Ramkesh Vats'

import os

from multiprocessing import Pool, Process, Queue, Pipe

import datetime

def basic_pool(x):
    print(x, '======, Process_id: ', os.getpid())
    assert type(x) == int
    return {x: x**x}


def basic_process(input_val):
    print('Hello {}, Have a nice day!'.format(input_val),
          '======, Process_id: ', os.getpid())


def process_with_queue(queue_object):
    queue_object.put(['YOYO', None, 89])


def process_with_pipe(pipe_object):
    pipe_object.send('Hello Python, its about to end {}'.
                     format(datetime.datetime.now().year))


if __name__ == '__main__':
    input_list = range(5)

    # Using Pool
    with Pool(os.cpu_count()) as pool_object:
        print(pool_object.map(basic_pool, input_list))

    # Using Process
    process_object = Process(target=basic_process, args=('Ramkesh',))
    process_object.start()
    process_object.join()

    # Process using queue as sharing object
    queue = Queue()
    queue.put('start object')
    process_object = Process(target=process_with_queue, args=(queue,))
    # Queue is empty here, so program will be in blocked state
    # if we does not put any element in queue.
    print('queue output before start: ', queue.get(timeout=5))
    process_object.start()
    print('queue output: ', queue.get())
    process_object.join()

    # Process using pipe as sharing object
    parent_end, child_end = Pipe()
    process_object = Process(target=process_with_pipe, args=(child_end,))

    process_object.start()
    print('Pipe output: ', parent_end.recv())
    process_object.join()