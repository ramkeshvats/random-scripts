from django.apps import AppConfig


class CompanyRssAndNewsConfig(AppConfig):
    name = 'company_RSS_and_News'
